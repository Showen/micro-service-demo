###
* @author Showen 03/14/16
###

define = require( 'amdefine' )( module ) if typeof define isnt 'function'

define [], () ->
  class MicroService
    constructor: ->
      console.log 'init..'
    start      : ( callback ) ->
      callback?()
    dispose    : ->
      console.log 'dispose...'

  module.exports =  exports =
    MicroService: MicroService
